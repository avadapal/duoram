
#ifndef DPFPP_PRG_H__
#define DPFPP_PRG_H__
 
namespace dpf
{

template<typename node_t, typename lowmc>
inline void PRG(const lowmc & prgkey, const node_t & seed, void * outbuf, const uint32_t len, const uint32_t from = 0);
 

} // namespace dpf
#endif // DPFPP_PRG_H
