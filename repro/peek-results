#!/bin/bash

# Peek at the results of an ongoing repro run, without touching the
# dockers.  Note: you will need to be able to run the parse_logs scripts
# on the *host* (i.e., you will need perl installed on the host),
# instead of in the dockers as usual.

# cd into the directory containing this script (from the bash faq 028)
if [[ $BASH_SOURCE = */* ]]; then
  cd -- "${BASH_SOURCE%/*}/" || exit
fi

numops=128
if [ "$1" != "" ]; then
    numops="$1"
fi

outputfile=$$.peek.log

export DUORAM_PARSE_HOST=1
export FLORAM_PARSE_HOST=1
export ORAM_PARSE_HOST=1

cleanup() {
    rm -f $outputfile
    exit
}

trap cleanup EXIT SIGINT

# Duoram
( cd ../Docker && ./repro none ${numops} ) > $outputfile

# Floram
( cd floram-docker && ./repro none ${numops} ) >> $outputfile

# Circuit ORAM
( cd circuit-oram-docker/docker && ./repro none ${numops} ) >> $outputfile

# Show stanzas starting with "# $1" and ending with "# " in the input
showstanza() {
    echo "# $1"
    sed -n "/# $1/,/# /{//!p;}"
}

echo
echo "===== Collated output ====="
echo

cat $outputfile | showstanza 'Figure 7(a)'
cat $outputfile | showstanza 'Figure 7(b)'
cat $outputfile | showstanza 'Figure 7(c)'
cat $outputfile | showstanza 'Figure 8(a)'
cat $outputfile | showstanza 'Figure 8(b)'
cat $outputfile | showstanza 'Figure 8(c)'
cat $outputfile | showstanza 'Figure 9(a)'
cat $outputfile | showstanza 'Figure 9(b)'
cat $outputfile | showstanza 'Figure 9(c)'
if [ "$DUORAM_EXTENDED_PLOTS" = "1" ]; then
    # Also show the plots for the extended version (Figures 11, 12)
    cat $outputfile | showstanza 'Figure 11(a)'
    cat $outputfile | showstanza 'Figure 11(b)'
    cat $outputfile | showstanza 'Figure 11(c)'
    cat $outputfile | showstanza 'Figure 12(a)'
    cat $outputfile | showstanza 'Figure 12(b)'
    cat $outputfile | showstanza 'Figure 12(c)'
fi
