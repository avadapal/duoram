

void du_attalah_P2(std::vector<socket_t>& sockets0, std::vector<socket_t>& sockets1, int socket_no = 0)
{
int64_t X0, X1, Y0, Y1,  gamma0, gamma1;

     arc4random_buf(&X0, sizeof(int64_t));
     arc4random_buf(&Y0, sizeof(int64_t));
     arc4random_buf(&X1, sizeof(int64_t));
     arc4random_buf(&Y1, sizeof(int64_t));

    gamma0 = X0 * Y1;
    gamma1 = X1 * Y0;
    
 

    boost::asio::write(sockets0[socket_no], boost::asio::buffer(&X0, sizeof(X0)));
    boost::asio::write(sockets1[socket_no], boost::asio::buffer(&X1, sizeof(X1)));
    
    boost::asio::write(sockets0[socket_no], boost::asio::buffer(&Y0, sizeof(Y0)));
    boost::asio::write(sockets1[socket_no], boost::asio::buffer(&Y1, sizeof(Y1)));
    
    boost::asio::write(sockets0[socket_no], boost::asio::buffer(&gamma0, sizeof(gamma0)));
    boost::asio::write(sockets1[socket_no], boost::asio::buffer(&gamma1, sizeof(gamma1)));
}

int64_t du_attalah_Pb(int64_t rb, int64_t pm, tcp::socket& s2, tcp::socket& sb)
{
    int64_t gamma;
	int64_t X, Y;
    int64_t rb_blinded, pm_blinded, pm_blinded_recv, rb_blinded_recv;
	boost::asio::read(s2, boost::asio::buffer(&X, sizeof(X)));
	boost::asio::read(s2, boost::asio::buffer(&Y, sizeof(Y)));
	boost::asio::read(s2, boost::asio::buffer(&gamma, sizeof(gamma)));
 

	rb_blinded = rb + X;
	pm_blinded = pm + Y;
	boost::asio::write(sb, boost::asio::buffer(&rb_blinded, sizeof(rb_blinded)));
	boost::asio::read(sb, boost::asio::buffer(&rb_blinded_recv, sizeof(rb_blinded_recv)));
	
	boost::asio::write(sb, boost::asio::buffer(&pm_blinded, sizeof(pm_blinded)));
	boost::asio::read(sb, boost::asio::buffer(&pm_blinded_recv, sizeof(pm_blinded_recv)));
	
	int64_t rb_prime = rb * (pm + (pm_blinded_recv)) - (Y * rb_blinded_recv) + gamma; 

    return rb_prime;
}