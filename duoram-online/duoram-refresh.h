/**
 * @brief This function is called by P2
 * 
 * @param flags 
 * @param standard_basis_vector 
 * @param finalCW 
 * @param party 
 * @param db_nitems 
 * @param rotate_by 
 */
void refresh_blinds(int8_t flags[], DB_t standard_basis_vector[], DB_t finalCW, bool party, size_t db_nitems, size_t rotate_by = 0)
{    
  DB_t tmp = 0; 
  for(size_t j = 0; j < db_nitems; ++j) 
  {
    tmp = (flags[(j + rotate_by) % db_nitems] != 0) ? finalCW : 0;

    if(party)  updated_blinds[j] = blinds[j] - standard_basis_vector[(j + rotate_by) % db_nitems]  - tmp;
    if(!party) updated_blinds[j] = blinds[j] - standard_basis_vector[(j + rotate_by) % db_nitems]  + tmp; 
  }
}

void refresh_blinded_vector_and_blinds(int8_t flags[], DB_t standard_basis_vector[], DB_t finalCW,int8_t flags0[], int8_t flags2[], DB_t b[],  DB_t d[], 
                                       bool party, size_t db_nitems, size_t rotate_by)
{
 for(size_t j = 0; j < db_nitems; ++j) 
 {
   updated_blinded_DB_recv[j] =  blinded_DB_recv[j] - b[(j + rotate_by) % db_nitems] + d[(j + rotate_by) % db_nitems]  
                               - (flags0[(j + rotate_by) % db_nitems] * finalCW) + (flags2[(j + rotate_by) % db_nitems] * finalCW);
                                      
   blinds[j] = blinds[j] - standard_basis_vector[(j + rotate_by) % db_nitems] - (flags[(j + rotate_by) % db_nitems] * finalCW); 
 }
}

void duoram_refresh (int8_t flags0[], int8_t flags1[], int8_t flags2[], DB_t b[], DB_t c[], DB_t d[],   DB_t CW, bool party, size_t db_nitems, size_t rotate_by_ = 0)
{ 
   refresh_blinded_vector_and_blinds(flags1, c, CW, flags0, flags2, b, d, party, db_nitems, rotate_by_);
}
