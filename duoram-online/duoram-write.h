void  write_into_the_database(DB_t DB[], DB_t updated_DB[], int8_t flags[], DB_t b[],  DB_t final_CW, bool party, size_t db_nitems, size_t rotate_by = 0)
{
 	for(size_t j = 0; j < db_nitems; ++j)
 	{ 
	  const size_t ind = (j + rotate_by) % db_nitems;
	 	DB_t tmp = (flags[ind] * final_CW);
	 	DB[j] = DB[j] + b[ind]  + tmp;     
		 updated_DB[j] = DB[j];
 	}
}
 
auto DuoramUpdate( bool party, size_t db_nitems,   size_t rotate_by_,
				               DB_t DB[], DB_t updated_DB[], int8_t writing_b[], DB_t b[],  DB_t final_CW, DB_t message,
             				  int8_t writing_c[], int8_t writing_d[], DB_t c[], DB_t d[])
{

	read_flags_for_updating(party, db_nitems);
	write_into_the_database(DB, updated_DB, writing_b, b, final_CW, party, db_nitems, - rotate_by_);
			
	#ifdef ThreeParty
		if(!party) duoram_refresh(writing_b, writing_c, writing_d, b, c, d,  final_CW, party, db_nitems, -rotate_by_); 			
		if( party) duoram_refresh(writing_b, writing_d, writing_c, b, d, c,  final_CW, party, db_nitems, -rotate_by_); 
	#endif
}