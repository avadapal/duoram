
auto DuoramRead(bool party, size_t db_nitems, int64_t ri, int64_t where_to_read, tcp::socket&s2, tcp::socket& s2_b, tcp::socket& sb)
{
 read_flags_for_reading(party, db_nitems);

 DB_t Gamma;
		
	size_t WritePb_ind_reads, WritePb_ind_reads_recv, rotate;
	WritePb_ind_reads = where_to_read - ri;
	
 boost::asio::write(s2, boost::asio::buffer(&WritePb_ind_reads, sizeof(WritePb_ind_reads)));
	boost::asio::write(sb, boost::asio::buffer(&WritePb_ind_reads, sizeof(WritePb_ind_reads)));
	boost::asio::read(sb, boost::asio::buffer(&WritePb_ind_reads_recv, sizeof(WritePb_ind_reads_recv)));
    
	rotate =  WritePb_ind_reads + WritePb_ind_reads_recv;

	#ifdef VERBOSE
		std::cout << "[dependent reads] " << " rotate = " << rotate << std::endl;
	#endif

	read(s2_b, boost::asio::buffer(&Gamma, sizeof(Gamma)));

	DB_t output = 0;
	   	
	for(size_t j = 0; j < db_nitems; ++j) reading_temp[j] = DB[j] + updated_blinded_DB_recv[j];
		 
	if(!party) output  =  dot_product_with_bool(reading_temp, reading_b, db_nitems, rotate )  +
						                 dot_product_with_bool(updated_blinds, reading_b, db_nitems, rotate) - 
						                 dot_product_with_bool(updated_blinds, reading_c, db_nitems, rotate) + Gamma;

	if(party)  output =   dot_product_with_bool(reading_temp, reading_c, db_nitems, rotate)   + 
			               			  dot_product_with_bool(updated_blinds, reading_c, db_nitems, rotate) - 
						                 dot_product_with_bool(updated_blinds, reading_d, db_nitems, rotate) + Gamma;


 return output;
}




auto DuoramIndependentRead(bool party, size_t db_nitems, int64_t ri, DB_t * Gamma_reads, size_t * rotate, size_t r)
{
	read_flags_for_reading(party, db_nitems); 

	DB_t output = 0;

	for(size_t j = 0; j < db_nitems; ++j) reading_temp[j] =  DB[j] + updated_blinded_DB_recv[j];
	
	if(!party) output =  dot_product_with_bool(reading_temp, 	reading_b, db_nitems, rotate[r]) + 
						               	dot_product_with_bool(updated_blinds, reading_b, db_nitems, rotate[r]) - 
							               dot_product_with_bool(updated_blinds, reading_c, db_nitems, rotate[r]) + Gamma_reads[r];

	if(party)  output =  dot_product_with_bool(reading_temp, 	reading_c, db_nitems, rotate[r]) + 
	               						dot_product_with_bool(updated_blinds, reading_c, db_nitems, rotate[r]) - 
							               dot_product_with_bool(updated_blinds, reading_d, db_nitems, rotate[r]) + Gamma_reads[r];

 return output;
}